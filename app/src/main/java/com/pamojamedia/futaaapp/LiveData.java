package com.pamojamedia.futaaapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Date;

/**
 * Created by Fidelia on 1/25/2016.
 */
public class LiveData {
    public int id;
    public String homeTeamImgUrl;
    public String awayTeamImgUrl;
    public String homeTeamName;
    public String awayTeamName;
    public String eventTime;
    public String eventUrl;
    public String eventInfo;


    public LiveData(){

    }

    public LiveData(int id, String eventInfo, String eventUrl, String eventTime, String awayTeamName, String homeTeamName, String awayTeamImgUrl, String homeTeamImgUrl) {
        this.id = id;
        this.eventInfo = eventInfo;
        this.eventUrl = eventUrl;
        this.eventTime = eventTime;
        this.awayTeamName = awayTeamName;
        this.homeTeamName = homeTeamName;
        this.awayTeamImgUrl = awayTeamImgUrl;
        this.homeTeamImgUrl = homeTeamImgUrl;
    }

    public String getHomeTeamImgUrl() {
        return homeTeamImgUrl;
    }

    public void setHomeTeamImgUrl(String homeTeamImgUrl) {
        this.homeTeamImgUrl = homeTeamImgUrl;
    }

    public String getAwayTeamImgUrl() {
        return awayTeamImgUrl;
    }

    public void setAwayTeamImgUrl(String awayTeamImgUrl) {
        this.awayTeamImgUrl = awayTeamImgUrl;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventUrl() {
        return eventUrl;
    }

    public void setEventUrl(String eventUrl) {
        this.eventUrl = eventUrl;
    }

    public String getEventInfo() {
        return eventInfo;
    }

    public void setEventInfo(String eventInfo) {
        this.eventInfo = eventInfo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}