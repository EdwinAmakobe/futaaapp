package com.pamojamedia.futaaapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

class LiveAdapter extends RecyclerView.Adapter<LiveAdapter.MyViewHolder1> {




    public Context context;
    public LayoutInflater liveInflater;
    public List<LiveData> liveDataList = new ArrayList<>();

    public LiveAdapter(Context context, List<LiveData> liveDataList) {
        liveInflater = LayoutInflater.from(context);
        this.context = context;
        this.liveDataList = liveDataList;
    }

    public LiveAdapter (){

    }

    @Override
    public MyViewHolder1 onCreateViewHolder(ViewGroup parent,  int viewType) {

        Context context = parent.getContext();
        LayoutInflater liveInflater = LayoutInflater.from(context);


            View view = liveInflater.inflate(R.layout.live_custom_row, parent, false);
            MyViewHolder1 holder = null;
            try {
                holder = new MyViewHolder1(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder1 holder, int i) {
       LiveData livecurrent = liveDataList.get(i);


            //setting the event values
            holder.id = livecurrent.getId();
            holder.eventTime = livecurrent.getEventTime();
            holder.eventUrl = livecurrent.getEventUrl();
            holder.homeTeamImgUrl = livecurrent.getHomeTeamImgUrl();
            holder.awayTeamImgUrl = livecurrent.getAwayTeamImgUrl();
            holder.homeTeamName.setText(livecurrent.getHomeTeamName());
            holder.awayTeamName.setText(livecurrent.getAwayTeamName());
            holder.eventInfo.setText(livecurrent.getEventInfo());

            if (holder.context != null) {
                Glide.with(holder.context).load(livecurrent.getHomeTeamImgUrl())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                return false;
                            }
                        }).override(700, 700)
                       // .placeholder(R.drawable.team1)
                        .into(holder.liveFirstImage);
            }
            //end glide

            //glide image loading 2
            //glide
            if (holder.context != null) {
                Glide.with(holder.context).load(livecurrent.getAwayTeamImgUrl())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                return false;
                            }
                        }).override(700, 700)
                       // .placeholder(R.drawable.team2)
                        .into(holder.liveSecondImage);
            }
            //end glide


        }



    @Override
    public int getItemCount() {
        return liveDataList == null ? 0 : liveDataList.size();
    }

    //get view type
    @Override
    public int getItemViewType(int position) {
            return 0;

    }

    public static class MyViewHolder1 extends RecyclerView.ViewHolder {

        public int view_type;
        public Context context;
        public TextView homeTeamName, awayTeamName, eventInfo, liveCategory;
        public ImageView liveFirstImage, liveSecondImage;
        public ProgressBar progressBar;
        public int id;
        public String homeTeamImgUrl;
        public String awayTeamImgUrl;
        public String eventUrl;
        public String eventTime;

        public MyViewHolder1(View itemView, int viewType, Context c) throws ParseException {
            super(itemView);
            context = itemView.getContext();
            itemView.setClickable(true);
                homeTeamImgUrl = "";
                awayTeamImgUrl = "";
                eventUrl = "";
                eventTime = "";

                //images
                liveFirstImage = (ImageView) itemView.findViewById(R.id.liveFirstImage);
                liveSecondImage = (ImageView) itemView.findViewById(R.id.liveSecondImage);
                //date
                eventInfo = (TextView) itemView.findViewById(R.id.eventInfo);
                //text views
                homeTeamName = (TextView) itemView.findViewById(R.id.homeTeamName);
                awayTeamName = (TextView) itemView.findViewById(R.id.awayTeamName);
                liveCategory = (TextView) itemView.findViewById(R.id.liveCategory);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //intent
                        Intent intent = new Intent(context, LiveDetails.class);
                        //bundling
                        Bundle bundle = new Bundle();
                        //strings
                        bundle.putString("awayTeamImgUrl", awayTeamImgUrl);
                        bundle.putString("homeTeamImgUrl", homeTeamImgUrl);
                        bundle.putString("eventInfo", eventInfo.getText().toString());
                        bundle.putString("liveCategory", liveCategory.getText().toString());
                        bundle.putString("liveTime", eventInfo.getText().toString());
                        bundle.putString("liveFirstName", homeTeamName.getText().toString());
                        bundle.putString("liveSecondName", awayTeamName.getText().toString());
                        bundle.putString("eventUrl", eventUrl);
                        bundle.putInt("id", id);
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });





            }


        }

    }







