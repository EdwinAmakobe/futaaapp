package com.pamojamedia.futaaapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Collections;
import java.util.List;

/**
 * Created by Edwin on 1/25/2016.
 */
public class LiveLiveAdapter extends RecyclerView.Adapter<LiveLiveAdapter.MyViewHolder> {
    public Context context;
    public static LayoutInflater liveliveInflater;
    List<LiveLiveData> data = Collections.EMPTY_LIST;

    public LiveLiveAdapter(Context context, List<LiveLiveData> data) {
        liveliveInflater = LayoutInflater.from(context);
        this.data = data;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            View view = liveliveInflater.inflate(R.layout.fragment_live_live_row, parent, false);
            MyViewHolder holder = new MyViewHolder(view, viewType, context);
            return holder;


    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
      final LiveLiveData current = data.get(position);


            holder.liveliveCommentsText.setText(current.details);
            holder.liveliveCommentsTime.setText(current.elapsed + "");
            holder.liveliveCommentsName.setText(current.code);

            //glide
            if (holder.context != null) {
                Glide.with(holder.context).load("http://futaa.com"+current.iconUrl)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                return false;
                            }
                        }).override(100, 100)
                        .placeholder(R.drawable.placeholder)
                        .into(holder.liveliveCommentsImgUrl);
            }
            //end glide

        //

        }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
       return 0;
    }

    //view holder
    class MyViewHolder extends RecyclerView.ViewHolder {

        public int view_type;
        public Context context;
       // TextView liveliveCommentsImgUrl;
        TextView liveliveCommentsName, liveliveCommentsTime, liveliveCommentsText;
        ImageView liveliveCommentsImgUrl;


        public MyViewHolder(final View itemView, int viewType, Context c) {
            super(itemView);
            context = itemView.getContext();

                liveliveCommentsImgUrl = (ImageView) itemView.findViewById(R.id.liveliveCommentsPic);
                //text views
                liveliveCommentsName = (TextView) itemView.findViewById(R.id.liveliveCommentsName);
                liveliveCommentsTime = (TextView) itemView.findViewById(R.id.liveliveCommentsTime);
                liveliveCommentsText = (TextView) itemView.findViewById(R.id.liveliveCommentsText);


            }
        }


    }







