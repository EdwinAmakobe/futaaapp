package com.pamojamedia.futaaapp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Fidelia on 1/15/2016.
 */
public class BlogF extends Fragment {

    public static String api_url = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/blogs";
    public static int i;
    public NewsAdapterBlog newsAdapter;
    public List<NewsData> list = Collections.EMPTY_LIST;
    ProgressBar newsProgressBar;
    RequestQueue requestQueue;
    RecyclerView newsRecyclerView;
    public SwipeRefreshLayout swipe;
    public  static String jsonString = "";

    public BlogF (){
    }

    //method to pass data
    private List<NewsData> parseResult(String result) throws ParseException, JSONException {
        //list to hold data
        List<NewsData> newsDataList = new ArrayList<>();

        //parse json string
        JSONObject newsItems = new JSONObject(result);

        //get array from json obj
        JSONArray newsArray = newsItems.optJSONArray("items");
        //loop through the other objects

        for (i = 0; i < newsArray.length(); i++) {

            //get the first object
            JSONObject header = newsArray.optJSONObject(0);
            NewsData newsdata = new NewsData();
            JSONObject data = newsArray.optJSONObject(i);

            //retrieving the values for the header
            String headerImage = header.optString("pic");
            String headerTitle = header.optString("title");
            String headerCategory = header.optString("url");
            String headerTime = header.optString("time");
            String date = data.getString("date");

            //setting the  values for variables
            newsdata.setHeaderImage(headerImage);
            newsdata.setHeaderTitle(headerTitle);
            newsdata.setHeaderCategory("Latest News");
            newsdata.setHeaderTime(date + " " + headerTime);

            int newsImage = R.id.newsImage;
            String newsTitle = data.getString("title");
            String newsCategory = "view details"; //to be replaced with category
            String newsTime = data.getString("time");
            // other vars in api
            String paragraph = data.getString("paragraph");
            int is_pinned = data.getInt("is_pinned");
            String detailsUrl = data.getString("url");
            int id = data.getInt("id");
            int pageNo = 1;
            String imgUrl  = data.optString("pic");


            //setting other values
            newsdata.setNewsImage(newsImage);
            newsdata.setNewsCategory(newsCategory);
            newsdata.setNewsTime(date + " " + newsTime);
            newsdata.setNewsTitle(newsTitle);

            newsdata.setImgUrl(imgUrl);
            newsdata.setPageNo(pageNo);
            newsdata.setId(id);
            newsdata.setParagraph(paragraph);
            newsdata.setDetailUrl(detailsUrl);
            newsDataList.add(newsdata);
        }

        return newsDataList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        final View view = inflater.inflate(R.layout.news_fragment, container, false);
        final FragmentActivity c = getActivity();
        newsRecyclerView = (RecyclerView) view.findViewById(R.id.news_recyclerview);
        newsProgressBar = (ProgressBar) view.findViewById(R.id.newsProgressBar);

        //setting a linear layout manager
        final LinearLayoutManager layoutManager = new LinearLayoutManager(c);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        newsRecyclerView.setLayoutManager(layoutManager);

        newsAdapter = new NewsAdapterBlog(getContext(), list);
        newsRecyclerView.setAdapter(newsAdapter);
        newsAdapter.notifyDataSetChanged();


        newsRecyclerView.setItemAnimator(new RecyclerView.ItemAnimator() {
            @Override
            public boolean animateDisappearance(RecyclerView.ViewHolder viewHolder, ItemHolderInfo preLayoutInfo, @Nullable ItemHolderInfo postLayoutInfo) {
                return false;
            }

            @Override
            public boolean animateAppearance(RecyclerView.ViewHolder viewHolder, @Nullable ItemHolderInfo preLayoutInfo, ItemHolderInfo postLayoutInfo) {
                return false;
            }

            @Override
            public boolean animatePersistence(RecyclerView.ViewHolder viewHolder, ItemHolderInfo preLayoutInfo, ItemHolderInfo postLayoutInfo) {
                return false;
            }

            @Override
            public boolean animateChange(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder, ItemHolderInfo preLayoutInfo, ItemHolderInfo postLayoutInfo) {
                return false;
            }

            @Override
            public void runPendingAnimations() {

            }

            @Override
            public void endAnimation(RecyclerView.ViewHolder item) {

            }

            @Override
            public void endAnimations() {

            }

            @Override
            public boolean isRunning() {
                return false;
            }
        });
        newsRecyclerView.setHasFixedSize(true);
        newsRecyclerView.setAdapter(newsAdapter);
        newsProgressBar.setVisibility(View.VISIBLE);

        //swiping
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipe.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        //volley
        requestQueue = VolleySingleton.getInstance().getRequestQueue();

        //cached volley
        CacheRequest cacheRequest = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                   String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                    try {
                        if (jsonString != null) {
                            newsAdapter = new NewsAdapterBlog(getContext(), parseResult(jsonString));
                            newsRecyclerView.setAdapter(newsAdapter);
                            newsAdapter.notifyDataSetChanged();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }newsProgressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                newsProgressBar.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                    Toast.makeText(getActivity(), "limited internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    //
                }
            }
        });

        // Add the request to the RequestQueue.
        requestQueue.add(cacheRequest);


        return view;
    }

    //refresh
    private void refreshContent() {
        CacheRequest refresh = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                   String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    if (jsonString != null) {
                        newsProgressBar.setVisibility(View.GONE);
                        newsAdapter = new NewsAdapterBlog(getActivity(), parseResult(jsonString));
                        newsRecyclerView.setAdapter(newsAdapter);
                        newsAdapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
                        //Toast.makeText(getActivity(), "Loading .... ", Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                newsProgressBar.setVisibility(View.GONE);
                swipe.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                   Toast.makeText(getActivity(), "limited connection ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "problem fetching data ", Toast.LENGTH_LONG).show();
                }


            }
        });

        // Add the request to the RequestQueue.
        swipe.setRefreshing(false);
        requestQueue.add(refresh);
    }

    //caching class
    class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 365 *24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
