package com.pamojamedia.futaaapp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class LiveComments extends Fragment {

    private RecyclerView liveCommentsRecyclerView;
    private LiveCommentsAdapter liveCommentsAdapter;
    private RecyclerView.LayoutManager LiveCommentsLayoutManager;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_live_comments, container,false);
        liveCommentsRecyclerView = (RecyclerView)view.findViewById(R.id.live_comments_recyclerview);
        //liveCommentsAdapter = new LiveCommentsAdapter(getActivity(), getData());
        liveCommentsRecyclerView.setAdapter(liveCommentsAdapter);
        liveCommentsRecyclerView.setHasFixedSize(true);
        liveCommentsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        liveCommentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        liveCommentsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        return view;
    }


    //getting the data
    public  static List<LiveCommentsData> getData(){
        List<LiveCommentsData> data = new ArrayList<>();

        String[] liveCommentsImgUrl={"Liverpool", "Arsenal", "Chelsea", "Arsenal", "Manchester United", "Chelsea"};
        String[] liveCommentsName={"10",  "20", "30", "44", "52", "78"};
        String[] liveCommentsTime = {"Red", "Yellow", "off", "Penalty", "Foul", "Free kick", "Timing"};
        String[] liveCommentsText = {"Red", "Yellow", "off", "Penalty", "Foul", "Free kick", "Timing"};


        for(int i=0; i<6; i++ ){
            LiveCommentsData current = new LiveCommentsData();
            current.liveCommentsImgUrl = liveCommentsImgUrl[i];
            current.liveCommentsName = liveCommentsName[i];
            current.liveCommentsTime = liveCommentsTime[i];
            current.liveCommentsText = liveCommentsText[i];
            data.add(current);

        }

        return data;

    }
}
