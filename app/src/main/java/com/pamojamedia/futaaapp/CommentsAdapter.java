package com.pamojamedia.futaaapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;

/**
 * Created by Fidelia on 1/25/2016.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    public static final int TYPE_LIST = 0;
    public static final int TYPE_AD = 1;
    public Context context;
    public LayoutInflater resultsInflater;
    List<CommentsData> data = Collections.EMPTY_LIST;

    public CommentsAdapter(Context context, List<CommentsData> data) {
        resultsInflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_LIST) {
            View view = resultsInflater.inflate(R.layout.results_cutom_row, parent, false);
            MyViewHolder holder = new MyViewHolder(view, viewType, context);
            return holder;
        } else if (viewType == TYPE_AD) {
            View view = resultsInflater.inflate(R.layout.advert_view, parent, false);
            MyViewHolder holder = new MyViewHolder(view, viewType, context);
            return holder;

        }
        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CommentsData current = data.get(position);

        if (holder.view_type == TYPE_LIST) {

            //setting the team names
            holder.resultsFirstName.setText(current.resultsFirstName);
            holder.resultsFirstImage.setImageResource(current.resultsFirstImage);


            //setting the time
            //holder.time.setText(current.startTime);
            // spacer
            holder.resultsSpacerImage.setImageResource(current.resultsSpacerImage);
            //scores
            holder.resultsFirstScore.setText(current.resultsFirstScore);
            holder.resultsSecondScore.setText(current.resultsSecondScore);

            //setting the image resources

            holder.resultsSecondImage.setImageResource(current.resultsSecondImage);
            holder.resultsSecondName.setText(current.resultsSecondName);

            holder.resultsCategory.setText(current.category);


            ////onclick listener


        } else if (holder.view_type == TYPE_AD) {
            //
        }

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position==1) {
            return TYPE_LIST;
        }
       else if (position == 2 || position == 8 || position == 14 || position == 20 || position == 26 || position == 32 || position == 38) {
            return TYPE_AD;
        } else {
            return TYPE_LIST;
        }
    }

    //view holder
    class MyViewHolder extends RecyclerView.ViewHolder {

        public int view_type;
        TextView resultsCategory;

        public Context context;
        TextView resultsFirstName, resultsSecondName, resultsFirstScore, resultsSecondScore;
        ImageView resultsFirstImage, resultsSecondImage, resultsSpacerImage;
        Bitmap bresultsFirstImage, bresultsSecondImage, bresultsSpacerImage;

        public MyViewHolder(final View itemView, int viewType, Context c) {
            super(itemView);
            context = itemView.getContext();

            if (viewType == TYPE_LIST) {

                //images
                resultsFirstImage = (ImageView) itemView.findViewById(R.id.resultsFirstImage);
                resultsSecondImage = (ImageView) itemView.findViewById(R.id.resultsSecondImage);
                resultsCategory =(TextView)itemView.findViewById(R.id.resultscategory);
                //date
                // time = (TextView)itemView.findViewById(R.id.startTime);

                //spacer
                resultsSpacerImage = (ImageView) itemView.findViewById(R.id.resultsSpacerImage);

                //scores
                resultsFirstScore = (TextView) itemView.findViewById(R.id.resultsFirstScore);
                resultsSecondScore = (TextView) itemView.findViewById(R.id.resultsSecondScore);


                //text views
                resultsFirstName = (TextView) itemView.findViewById(R.id.resultsFirstName);
                resultsSecondName = (TextView) itemView.findViewById(R.id.resultsSecondName);


                //handling an item click
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //preparing images for passing
                        //converting imageViews to bitmaps
                        resultsFirstImage.buildDrawingCache();
                        Bitmap bresultsFirstImage = resultsFirstImage.getDrawingCache();
                        BitmapFactory.decodeResource(context.getResources(), R.id.resultsFirstImage);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bresultsFirstImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] firstImage = stream.toByteArray();


                        resultsSecondImage.buildDrawingCache();
                        Bitmap bresultsSecondImage = resultsSecondImage.getDrawingCache();
                        BitmapFactory.decodeResource(context.getResources(), R.id.resultsSecondImage);
                        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                        bresultsSecondImage.compress(Bitmap.CompressFormat.PNG, 100, stream1);
                        byte[] secondImage = stream1.toByteArray();


//



                    }
                });
                view_type=1;

            } else if(viewType==TYPE_AD) {
                //code for implementing adview
                view_type = 3;

            }
        }


    }

}





