package com.pamojamedia.futaaapp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsDetailsComments extends Fragment {

    private RecyclerView newsCommentsRe;
    private NewsDetailsCommentsAdapter newsDetailsCommentsAdapter;
    private RecyclerView.LayoutManager newsCommentsLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_live_comments_row,container,false);
        newsCommentsRe = (RecyclerView)view.findViewById(R.id.live_comments_recyclerview);

        try {
            newsDetailsCommentsAdapter = new NewsDetailsCommentsAdapter(getActivity(), getData());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        newsCommentsRe.setAdapter(newsDetailsCommentsAdapter);
        newsCommentsRe.setHasFixedSize(true);
        newsCommentsRe.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        newsCommentsRe.setLayoutManager(new LinearLayoutManager(getActivity()));
        newsCommentsRe.setItemAnimator(new DefaultItemAnimator());
        return view;
    }



    //getting the data
    public  static List<NewsDetailsCommentsData> getData() throws ParseException {
        List<NewsDetailsCommentsData> data = new ArrayList<>();

        int[] newsImages = {R.drawable.news1, R.drawable.news2,
                R.drawable.news1, R.drawable.news2, R.drawable.wanyama_news, R.drawable.news1, R.drawable.news1, R.drawable.news2,
                R.drawable.news1, R.drawable.news2, R.drawable.wanyama_news, R.drawable.news1};

        String[] newsTitles = {"Shrewsbury have been drawn at home to Manchester United in the FA Cup fifth round.",
                "Chelsea have been paired with Man City in round five",
                "Terry's guile and cunning made him a chelsea hero and might just get him.",
                "Nyamweya congratulates stakeholders for peaceful polls", "stakeholders for peaceful polls",
                "Nyamweya congratulates stakeholders for peaceful polls", "Shrewsbury have been drawn at home to Manchester United in the FA Cup fifth round.", "Chelsea have been paired with Man City in round five", "Terry's guile and cunning made him a chelsea hero and might just get him.", "Nyamweya congratulates stakeholders for peaceful polls", "stakeholders for peaceful polls", "Nyamweya congratulates stakeholders for peaceful polls"};

        String[] newsCategorys = {"English Premier League", "FKF Elections", "English Premier League", "Team Information", "Matches Ahead", "Kenya Premier League", "Transfers", "FKF Elections", "English Premier League", "Team Information", "Matches Ahead", "Kenya Premier League"};




        //getting time from the date
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2016-02-14");



        String newsTime = dateFormat.format(date);


        String[] newsTimes = {newsTime, newsTime, newsTime, newsTime, newsTime, newsTime, newsTime, newsTime, newsTime, newsTime, newsTime, newsTime};


        for (int i = 0; i < newsImages.length && i < newsTitles.length && i < newsCategorys.length && i < newsTimes.length; i++) {
            NewsDetailsCommentsData current = new NewsDetailsCommentsData();
            //other items
            current.newsCommentsPic = newsImages[i];
            current.newsCommentsName = newsCategorys[i];
            current.newsCommentsTime= newsTimes[i];
            current.newsCommentsText=  newsTitles[i];
            data.add(current);
        }
        return data;

    }

}
