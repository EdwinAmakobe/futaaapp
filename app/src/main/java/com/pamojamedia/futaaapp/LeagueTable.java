package com.pamojamedia.futaaapp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Fidelia on 1/15/2016.
 */
public class LeagueTable extends Fragment {

    private static int i;
    ProgressBar progressBar;
    RequestQueue requestQueue;
    public static String api_url;
    public String base_url ="http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/league_standing/";
    public String lname;
    private SwipeRefreshLayout swipe;
    public static RecyclerView tableRecyclerView;
    public static LeagueListingAdapter tableAdapter;
    public List<LeagueListingData> list = Collections.EMPTY_LIST;
    public  static String jsonString = "";

    public LeagueTable (){
    }




    private List<LeagueListingData> parseResult(String jsonString) throws ParseException, JSONException {
        List<LeagueListingData> listTable = new ArrayList<>();

        JSONArray tableArray = new JSONArray(jsonString);
        //loop through the other objects

        for (i = 0; i < tableArray.length(); i++) {
            LeagueListingData tabledata = new LeagueListingData();
            JSONObject data = tableArray.optJSONObject(i);

            String teamName = data.getString("team");
            int rank = data.getInt("rank");
            int played = data.getInt("played");
            int goalsFor = data.getInt("goalsfor");
            int goalsAgainst= data.getInt("goalsagainst");
            int goalDiff = data.getInt("goaldifference");
            int points = data.getInt("points");

            //setting the variables
            tabledata.setRank(rank);
            tabledata.setTeamName(teamName);
            tabledata.setPlayed(played);
            tabledata.setGoals(goalsFor);
            tabledata.setConceded(goalsAgainst);
            tabledata.setGoalDiff(goalDiff);
            tabledata.setPoints(points);

            listTable.add(tabledata);

        }

        return listTable;
    }


    //parsing

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.table_fragment, container, false);
        tableRecyclerView = (RecyclerView) view.findViewById(R.id.table_recyclerview);
        tableAdapter = new LeagueListingAdapter(getContext(), list);
        tableRecyclerView.setAdapter(tableAdapter);
        tableAdapter.notifyDataSetChanged();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        tableRecyclerView.setLayoutManager(layoutManager);
        tableRecyclerView.setItemAnimator(new DefaultItemAnimator());
        tableRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipe.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        //volley
        requestQueue = VolleySingleton.getInstance().getRequestQueue();


        LeagueDetails ld = new LeagueDetails();
        lname = ld.getLname();
        api_url = base_url+lname;

        //cached volley
        CacheRequest cacheRequest = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    progressBar.setVisibility(View.GONE);
                    try {
                        tableAdapter = new LeagueListingAdapter(getContext(), parseResult(jsonString));
                        tableRecyclerView.setAdapter(tableAdapter);
                        tableAdapter.notifyDataSetChanged();
                    } catch (ParseException | JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {

                } else {

                }
            }
        });

        // Add the request to the RequestQueue.
        requestQueue.add(cacheRequest);

        return view;
    }

    //refresh
    private void refreshContent() {
        CacheRequest refresh = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                    progressBar.setVisibility(View.GONE);
                    tableAdapter = new LeagueListingAdapter(getActivity(), parseResult(jsonString));
                    tableRecyclerView.setAdapter(tableAdapter);
                    tableAdapter.notifyDataSetChanged();
                    swipe.setRefreshing(false);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                swipe.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {

                } else {

                }


            }
        });

        // Add the request to the RequestQueue.
        swipe.setRefreshing(false);
        requestQueue.add(refresh);
    }

    //parsing
    //getting the data


    //caching class
    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3*60* 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 365 *24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
