package com.pamojamedia.futaaapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;


public class NewsAdapterBlog extends RecyclerView.Adapter<NewsAdapterBlog.MyViewHolder> {


    public static final int TYPE_HEAD = 0;
    public static final int TYPE_LIST = 1;
    public static final int TYPE_AD = 3;

    //variable
    public Context context;
    public static LayoutInflater newsInflater;
    List<NewsData> data = Collections.EMPTY_LIST;


    public NewsAdapterBlog(Context context, List<NewsData> data) {
        newsInflater = LayoutInflater.from(context);
        this.context = context;
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_LIST) {
            View view = newsInflater.inflate(R.layout.news_custom_row, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        } else if (viewType == TYPE_HEAD) {
            View view = newsInflater.inflate(R.layout.news_custom_header, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        } else if (viewType == TYPE_AD) {
            View view = newsInflater.inflate(R.layout.advert_view, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final NewsData current = data.get(position);
        if (holder.view_type == TYPE_LIST) {
            holder.imgUrl = current.getImgUrl();
            holder.id = current.getId();

            //glide image loading
            //glide
            if (holder.context != null) {
                Glide.with(holder.context).load(current.getImgUrl())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                holder.newsProgressBar.setVisibility(View.GONE);
                                return false;
                            }
                        }).override(800, 530)
                        .placeholder(R.drawable.placeholder)
                        .into(holder.newsImage);
            }
            //end glide

            //glide image loading

            holder.newsTitle.setText(current.newsTitle);
            holder.newsCategory.setText(current.newsCategory);
            holder.newsTime.setText(current.newsTime);

            //new variables
            holder.detailsUrl = current.getDetailUrl();

            holder.paragraph = current.getParagraph();


        } else if (holder.view_type == TYPE_HEAD) {
            holder.imgUrl = current.getHeaderImage();
            holder.id = current.getId();

            //glide image loading
            //glide
            if (holder.context != null) {
                Glide.with(holder.context).load(holder.imgUrl)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                //holder.newsProgressBar.setVisibility(View.GONE);
                                return false;
                            }
                        }).override(800, 530)
                        .placeholder(R.drawable.placeholder)
                        .into(holder.headerImage);
            }
            //end glide

            //glide image loading

            holder.headerCategory.setText(current.headerCategory);
            holder.headerTitle.setText(current.headerTitle);
            holder.headerTime.setText(current.headerTime);
            holder.headerTime.setVisibility(View.GONE);

            //new variables
            holder.detailsUrl = current.getDetailUrl();

            holder.paragraph = current.getParagraph();


        } else if (holder.view_type == TYPE_AD) {

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //get view type
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEAD;
        } else if (position == 1) {
            return TYPE_LIST;
        } else if (position == 5) {
            return TYPE_LIST;
        } else if (position == 2 || position == 8 || position == 14 || position == 20 || position == 26 || position == 32 || position == 38) {
            return TYPE_AD;
        } else {
            return TYPE_LIST;
        }

    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        //variable for selecting the view type
        public int view_type;
        public Context context;
        RelativeTimeTextView newsTime;
        TextView newsTitle, newsCategory;
        TextView headerTitle, headerCategory, headerTime;
        ImageView newsImage, headerImage;
        String paragraph = "no content for this article";
        String detailsUrl = "http://www.futaa.com";
        String imgUrl = "http://www.futaa.com//images/olumm.jpg";
        String base_url = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/blog/";
        ProgressBar newsProgressBar;
        int id;

        public MyViewHolder(View itemView, int viewType, Context c) throws ParseException {
            super(itemView);
            context = itemView.getContext();
            itemView.setClickable(true);
            if (viewType == TYPE_LIST) {
                //images
                newsImage = (ImageView) itemView.findViewById(R.id.newsImage);

                // mAttacher = new PhotoViewAttacher(newsImage);
                newsProgressBar = (ProgressBar) itemView.findViewById(R.id.newsProgressBar);
                //title
                newsTitle = (TextView) itemView.findViewById(R.id.newsTitle);


                //listener for title
                newsTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, NewsDetails.class);
                        //bundling
                        Bundle bundle = new Bundle();

                        //strings
                        bundle.putString("imgUrl", imgUrl);
                        bundle.putString("detailsUrl", detailsUrl);
                        bundle.putString("paragraph", paragraph);
                        bundle.putString("newsTime", newsTime.getText().toString());
                        bundle.putString("newsCategory", newsCategory.getText().toString());
                        bundle.putString("newsTitle", newsTitle.getText().toString());
                        bundle.putInt("id", id);
                        bundle.putString("base_url", base_url);
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });
                //listener for title end


                //time ago library
                newsTime = (RelativeTimeTextView) itemView.findViewById(R.id.newsTime);//Or just me to simplify work!
//                String inputStr = "16-03-2016 06:34 AM";
//                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
//                long inputDate = dateFormat.parse(inputStr).getTime();
//                newsTime.setReferenceTime(inputDate);


                //category
                newsCategory = (TextView) itemView.findViewById(R.id.newsCategory);
                newsCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent readOnline = new Intent(context, ReadOnline.class);
                        Bundle b = new Bundle();
                        b.putString("detailsUrl", detailsUrl);
                        b.putInt("id", id);
                        readOnline.putExtras(b);
                        context.startActivity(readOnline);
                    }
                });
                view_type = 1;


            } else if (viewType == TYPE_HEAD) {

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //intent
                        Intent intent = new Intent(context, NewsDetails.class);
                        //bundling
                        Bundle bundle = new Bundle();

                        //strings
                        bundle.putString("imgUrl", imgUrl);
                        bundle.putString("detailsUrl", detailsUrl);
                        bundle.putString("paragraph", paragraph);
                        bundle.putString("newsTime", headerTime.getText().toString());
                        bundle.putString("newsCategory", headerCategory.getText().toString());
                        bundle.putString("newsTitle", headerTitle.getText().toString());
                        bundle.putInt("id", id);
                        bundle.putString("base_url", base_url);
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });

                //images
                headerImage = (ImageView) itemView.findViewById(R.id.headerImage);
                // mAttacher = new PhotoViewAttacher(headerImage);

                //title
                headerTitle = (TextView) itemView.findViewById(R.id.headerTitle);


                //time ago library
                headerTime = (RelativeTimeTextView) itemView.findViewById(R.id.headerTime);
                headerTime.setVisibility(View.GONE);
                //Or just me to simplify work!


                //category
                headerCategory = (TextView) itemView.findViewById(R.id.headerCategory);
                view_type = 0;


            } else if (viewType == TYPE_AD) {

                view_type = 3;
            }


        }


    }


}







