package com.pamojamedia.futaaapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

public class NewsDetailsCommentsAdapter extends RecyclerView.Adapter<NewsDetailsCommentsAdapter.MyViewHolder> {

    // PhotoViewAttacher mAttacher;
    public static final int TYPE_LIST = 1;
    public static final int TYPE_AD = 3;
    public LayoutInflater newsCommentsInflater;
    //variable
    Context context;
    List<NewsDetailsCommentsData> data = Collections.EMPTY_LIST;


    public NewsDetailsCommentsAdapter(Context context, List<NewsDetailsCommentsData> data) {
        newsCommentsInflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_LIST) {
            View view = newsCommentsInflater.inflate(R.layout.news_details_comments_row, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        }  else if (viewType == TYPE_AD) {
            View view = newsCommentsInflater.inflate(R.layout.advert_view, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewsDetailsCommentsData current = data.get(position);
        if (holder.view_type == TYPE_LIST) {

            //setting the news items
            holder.newsCommentsPic.setImageResource(current.newsCommentsPic);
            // mAttacher.update();
            holder.newsCommentsText.setText(current.newsCommentsText);
            holder.newsCommentsName.setText(current.newsCommentsName);
            //setting the time
            holder.newsCommentsTime.setText(current.newsCommentsTime);
            //  handling clicks for item view


        } else if (holder.view_type == TYPE_AD) {
            //

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //get view type
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_LIST;
        } else {
            return TYPE_LIST;
        }

    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        //variable for selecting the view type
        public int view_type;
        public Context context;
        RelativeTimeTextView newsCommentsTime;
        TextView newsCommentsName, newsCommentsText;
        ImageView newsCommentsPic;


        public MyViewHolder(View itemView, int viewType, Context c) throws ParseException {
            super(itemView);
            context = itemView.getContext();
            itemView.setClickable(true);
            if (viewType == TYPE_LIST) {

//                itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        int path=  R.id.newsImage;
//
//                        newsImage.buildDrawingCache();
//                        Bitmap newsHeaderImage = newsImage.getDrawingCache();
//                        BitmapFactory.decodeResource(context.getResources(), R.id.newsImage);
//                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                        newsHeaderImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                        byte[] firstImage = stream.toByteArray();
//
//
//                        //intent
//                        Intent intent = new Intent(context, NewsDetails.class);
//                        //bundling
//                        Bundle bundle = new Bundle();
//                        //adding bitmaps to intent.
//                        intent.putExtra("newsImage", firstImage);
//                        bundle.putInt("newsImageId", path);
//
//                        //strings
//                        bundle.putString("newsTime", newsTime.getText().toString());
//                        bundle.putString("newsCategory", newsCategory.getText().toString());
//                        bundle.putString("newsTitle", newsTitle.getText().toString());
//                        intent.putExtras(bundle);
//                        context.startActivity(intent);
//
//                    }
//                });
                //images
                newsCommentsPic = (ImageView) itemView.findViewById(R.id.newsImage);

                // mAttacher = new PhotoViewAttacher(newsImage);

                //title
                newsCommentsText = (TextView) itemView.findViewById(R.id.newsTitle);


                //time ago library
                newsCommentsTime = (RelativeTimeTextView) itemView.findViewById(R.id.newsTime);//Or just me to simplify work!
                String inputStr = "16-02-2016 06:34 AM";
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                long inputDate = dateFormat.parse(inputStr).getTime();
                newsCommentsTime.setReferenceTime(inputDate);

                //category
                newsCommentsName = (TextView) itemView.findViewById(R.id.newsCategory);
                view_type = 1;


            } else if (viewType == TYPE_AD) {
                view_type = 3;
            }


        }


    }


}







