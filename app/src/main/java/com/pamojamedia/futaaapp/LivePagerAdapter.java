package com.pamojamedia.futaaapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Fidelia on 2/11/2016.
 */
public class LivePagerAdapter extends FragmentStatePagerAdapter {

    CharSequence liveTitles[] = {"Teams", "Comments", "Live"} ;
    int noOfTabs = 3 ; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public LivePagerAdapter(FragmentManager fm,CharSequence liveTitles[], int noOfTabs) {
        super(fm);

        this.liveTitles = liveTitles;
        this.noOfTabs = noOfTabs;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (position == 0) // if the position is 0 we are returning the First tab
        {
            LiveTeams liveTeams = new LiveTeams();
            return liveTeams;
        } else if (position == 1)           // if the position is 1 we are returning the First tab
        {
            LiveComments liveComments = new LiveComments();
            return liveComments;
        } else {                             //if the position is 2 we are returning the First tab

            LiveLive liveLive = new LiveLive();
            return liveLive;
        }

    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return liveTitles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return noOfTabs;
    }
}
