package com.pamojamedia.futaaapp;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Blog extends AppCompatActivity {

    public final static String api_url = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/blogs";
    private static final String TAG = "Completed";
    public static String jsonString = "";
    public List<BlogData> blogList;
    public RecyclerView blogRecyclerView;
    public BlogAdapterBlog blogAdapter;
    RequestQueue requestQueue;
    private ProgressBar blogProgressBar;
    private SwipeRefreshLayout swipe;

    public Blog (){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //initialization

        blogRecyclerView = (RecyclerView) findViewById(R.id.blogRecyclerview);
        blogRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        blogProgressBar = (ProgressBar) findViewById(R.id.blogProgressBar);
        blogProgressBar.setVisibility(View.VISIBLE);


        //swiping
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipe.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });


        //swiping
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        //cached volley
        CacheRequest cacheRequest = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    parseResult(jsonString);
                    blogProgressBar.setVisibility(View.GONE);
                    blogAdapter = new BlogAdapterBlog(Blog.this, blogList);
                    blogRecyclerView.setAdapter(blogAdapter);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                blogProgressBar.setVisibility(View.GONE);

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                    Toast.makeText(Blog.this, "No connection ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(Blog.this, "problem fetching data ", Toast.LENGTH_LONG).show();
                }

            }
        });

        // Add the request to the RequestQueue.
        requestQueue.add(cacheRequest);


    }

    private void refreshContent() {

        CacheRequest refresh = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    parseResult(jsonString);
                    blogProgressBar.setVisibility(View.GONE);
                    blogAdapter = new BlogAdapterBlog(Blog.this, blogList);
                    blogRecyclerView.setAdapter(blogAdapter);
                    swipe.setRefreshing(false);
                    Toast.makeText(Blog.this, "content refreshed", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                blogProgressBar.setVisibility(View.GONE);
                swipe.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                    Toast.makeText(Blog.this, "No connection ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(Blog.this, "problem fetching data ", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Add the request to the RequestQueue.
        swipe.setRefreshing(false);
        requestQueue.add(refresh);
    }

    //parsing
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray data = response.optJSONArray("items");
            blogList = new ArrayList<>();

            for (int i = 0; i < data.length(); i++) {
                JSONObject blogdata = data.optJSONObject(i);
                BlogData item = new BlogData();

                //getting the values
                String blogTitle = blogdata.optString("title");
                String blogCategory = "view details";
                String blogTime = blogdata.optString("time");
                String blogDate = blogdata.optString("date");

                String detailsUrl = blogdata.optString("url");
                String paragraph = blogdata.optString("paragraph");
                String imgUrl  = blogdata.optString("pic");
                int pageNo = 1;
                int id = blogdata.optInt("id");
                String is_pinned = blogdata.optString("is_pinned");

                //setting values
                item.setImgUrl(imgUrl);
                item.setBlogCategory(blogCategory);
                item.setBlogTitle(blogTitle);
                item.setBlogTime(blogDate + " " + blogTime);
                item.setParagraph(paragraph);
                item.setDetailsUrl(detailsUrl);
                item.setIs_pinned(is_pinned);
                item.setPageNo(pageNo);
                item.setId(id);

                blogList.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //caching

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }

    //caching class
    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

}

