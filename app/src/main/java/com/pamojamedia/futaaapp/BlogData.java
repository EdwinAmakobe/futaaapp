package com.pamojamedia.futaaapp;

/**
 * Created by Edwin on 3/2/2016.
 */
public class BlogData {

    //header



    //header

    String blogTitle;
    String blogCategory;
    String blogTime;
    String detailsUrl;
    String paragraph;
    String imgUrl;
    int pageNo;
    int id;
    String is_pinned;

    public BlogData(String blogTitle, String is_pinned, int id, int pageNo, String paragraph, String imgUrl, String detailsUrl, String blogCategory, String blogTime) {
        this.blogTitle = blogTitle;
        this.is_pinned = is_pinned;
        this.id = id;
        this.pageNo = pageNo;
        this.paragraph = paragraph;
        this.imgUrl = imgUrl;
        this.detailsUrl = detailsUrl;
        this.blogCategory = blogCategory;
        this.blogTime = blogTime;
    }

    public BlogData() {
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public String getBlogCategory() {
        return blogCategory;
    }

    public void setBlogCategory(String blogCategory) {
        this.blogCategory = blogCategory;
    }

    public String getBlogTime() {
        return blogTime;
    }

    public void setBlogTime(String blogTime) {
        this.blogTime = blogTime;
    }

    public String getDetailsUrl() {
        return detailsUrl;
    }

    public void setDetailsUrl(String detailsUrl) {
        this.detailsUrl = detailsUrl;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIs_pinned() {
        return is_pinned;
    }

    public void setIs_pinned(String is_pinned) {
        this.is_pinned = is_pinned;
    }
}
