package com.pamojamedia.futaaapp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Fidelia on 1/15/2016.
 */
public class Upcoming extends News {

    public static int i;

    ProgressBar progressBar;
    public RequestQueue requestQueue;
    public static String api_url = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/upcoming";
    private SwipeRefreshLayout swipe;
    public RecyclerView liveRecyclerView;
    public static LiveAdapter liveAdapter;
    public List<LiveData> list = Collections.EMPTY_LIST;
    public  static String jsonString = "";

    public static  Fragment newInstance (){
        Upcoming upcoming = new Upcoming();
        return upcoming;
    }

    private List<LiveData> parseResult(String result) throws ParseException, JSONException {
        List<LiveData> liveDataList = new ArrayList<>();

        JSONObject liveItems = new JSONObject(result);

        //get array from json obj
        // Toast.makeText(Live.this, "gh", Toast.LENGTH_SHORT).show();
        JSONArray liveArray = liveItems.optJSONArray("events");
        //loop through the other objects


        for (i = 0; i < liveArray.length(); i++) {
            LiveData livedata = new LiveData();
            JSONObject data = liveArray.optJSONObject(i);

            //getting the variables
            String homeTeamImgUrl = "https://lh4.googleusercontent.com/-dZ2LhrpNpxs/AAAAAAAAAAI/AAAAAAAAy-M/iHnTK_2LdSA/s0-c-k-no-ns/photo.jpg";
            String awayTeamImgUrl = "https://lh3.googleusercontent.com/-iDzlv7IG4rY/AAAAAAAAAAI/AAAAAAACm-I/vS-3cffMf7E/s0-c-k-no-ns/photo.jpg";
            String eventInfo = data.getString("info");
            String eventUrl = data.getString("url");
            String homeTeamName = data.getString("home_team");
            String awayTeamName = data.getString("away_team");
            String eventTime = data.getString("time");
            int id = data.getInt("id");


            //setting the variables
            livedata.setId(id);
            livedata.setHomeTeamName(homeTeamName);
            livedata.setAwayTeamName(awayTeamName);
            livedata.setHomeTeamImgUrl(homeTeamImgUrl);
            livedata.setAwayTeamImgUrl(awayTeamImgUrl);
            livedata.setEventUrl(eventUrl);
            livedata.setEventTime(eventTime);
            livedata.setEventInfo(eventTime);

            //Toast.makeText(getContext(), homeTeamName, Toast.LENGTH_SHORT).show();
            liveDataList.add(livedata);

        }


        return liveDataList;

    }


    //parsing

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.live_fragment, container, false);
        liveRecyclerView = (RecyclerView) view.findViewById(R.id.live_recyclerview);
        liveAdapter = new LiveAdapter(getContext(), list);
        liveRecyclerView.setAdapter(liveAdapter);
        liveAdapter.notifyDataSetChanged();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        liveRecyclerView.setLayoutManager(layoutManager);
        liveRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //liveRecyclerView.setHasFixedSize(false);



        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);        //swiping
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipe.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        //volley
        requestQueue = VolleySingleton.getInstance().getRequestQueue();

        //cached volley
        CacheRequest cacheRequest = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    progressBar.setVisibility(View.GONE);
                    try {
                        liveAdapter = new LiveAdapter(getContext(), parseResult(jsonString));
                        liveRecyclerView.setAdapter(liveAdapter);
                        liveAdapter.notifyDataSetChanged();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {

                } else {

                }
            }
        });

        // Add the request to the RequestQueue.
        requestQueue.add(cacheRequest);

        return view;
    }

    //refresh
    private void refreshContent() {
        CacheRequest refresh = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));


                    progressBar.setVisibility(View.GONE);
                    liveAdapter = new LiveAdapter(getActivity(), parseResult(jsonString));
                    liveRecyclerView.setAdapter(liveAdapter);
                    liveAdapter.notifyDataSetChanged();
                    swipe.setRefreshing(false);

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                swipe.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {

                } else {

                }


            }
        });

        // Add the request to the RequestQueue.
        swipe.setRefreshing(false);
        requestQueue.add(refresh);
    }

    //parsing
    //getting the data


    //caching class
    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 365 *24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
