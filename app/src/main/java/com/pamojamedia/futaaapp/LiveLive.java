package com.pamojamedia.futaaapp;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Fidelia on 1/15/2016.
 */
public class LiveLive extends Fragment {

    public static int i;
    public LiveLiveAdapter liveliveAdapter;
    public static String api_url;
    ProgressBar progressBar;
    RequestQueue requestQueue;
    private RecyclerView liveliveRecyclerView;
    private SwipeRefreshLayout swipe;
    public String base_url = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/event/";
    int api_id;
    public static String jsonString="";

    //method to pass data
    private List<LiveLiveData> parseResult(String result) throws ParseException, JSONException {
        List<LiveLiveData> liveDataList = new ArrayList<>();

        JSONObject liveItems = new JSONObject(result);

        //get array from json obj
        // Toast.makeText(Live.this, "gh", Toast.LENGTH_SHORT).show();
        JSONArray liveArray = liveItems.optJSONArray("occurences");

        //loop through the other objects

        if(liveArray!=null) {
            for (i = 0; i < liveArray.length(); i++) {
                LiveLiveData livelivedata = new LiveLiveData();
                JSONObject data = liveArray.optJSONObject(i);

                //getting the variables
                livelivedata.code = data.getString("code");
                livelivedata.details = data.getString("details");
                livelivedata.iconUrl = data.getString("icon");
                livelivedata.elapsed = data.getInt("elapsed");
                liveDataList.add(livelivedata);
                //
            }

        }

       // start
        // ArrayList<LiveLiveData> liveDataList1 =((ArrayList<LiveLiveData>)liveDataList);
        // Arrays.asList(liveDataList);
        Collections.sort(liveDataList);
        //end
        return liveDataList;

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_live_live, container, false);
        liveliveRecyclerView = (RecyclerView) view.findViewById(R.id.liveliveRecyclerview);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        LiveDetails ld = (LiveDetails) getActivity();
        api_id = ld.getApi_id();
        api_url = base_url+api_id;

        //setting a linear layout manager
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        liveliveRecyclerView.setLayoutManager(layoutManager);


        liveliveRecyclerView.setItemAnimator(new DefaultItemAnimator());
        liveliveRecyclerView.setHasFixedSize(true);
        liveliveRecyclerView.setAdapter(liveliveAdapter);
        //liveliveAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);


        //swiping
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipe.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        //volley
        requestQueue = VolleySingleton.getInstance().getRequestQueue();


       //cached volley
        CacheRequest cacheRequest = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                    try {

                        liveliveAdapter = new LiveLiveAdapter(getContext(), parseResult(jsonString));
                        liveliveRecyclerView.setAdapter(liveliveAdapter);
                        liveliveAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        // Add the request to the RequestQueue.
        requestQueue.add(cacheRequest);


        return view;
    }


    //refresh
    private void refreshContent() {
        CacheRequest refresh = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    progressBar.setVisibility(View.GONE);
                    liveliveAdapter = new LiveLiveAdapter(getContext(), parseResult(jsonString));
                    liveliveRecyclerView.setAdapter(liveliveAdapter);
                    liveliveAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Loading .... ", Toast.LENGTH_SHORT).show();
                    swipe.setRefreshing(false);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                swipe.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                    Toast.makeText(getActivity(), "No connection ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "problem fetching data ", Toast.LENGTH_LONG).show();
                }


            }
        });

        // Add the request to the RequestQueue.

        requestQueue.add(refresh);
    }

    //caching class
    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
