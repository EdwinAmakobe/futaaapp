package com.pamojamedia.futaaapp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fidelia on 1/15/2016.
 */
public class LiveTeams extends Fragment {

    public static int i;
    ProgressBar progressBar;
    RequestQueue requestQueue;
    public static String api_url;
    public String base_url = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/event/";
    TextView hometeam, awayteam, homescore, awayscore, league, starttime, status;
    String phometeam, pawayteam, phomescore, pawayscore, pleague, pstarttime, pstatus;
    int api_id;
    public static String jsonString="";

    public LiveTeams (){
    }

    //method to pass data
    private void parseResult(String result) throws ParseException, JSONException {

        JSONObject liveObj = new JSONObject(result);
        JSONObject infoObj = liveObj.optJSONObject("info");
        phometeam = infoObj.optString("home_team");
        pawayteam = infoObj.optString("away_team");
        phomescore = infoObj.optString("livescore_home");
        pawayscore = infoObj.optString("livescore_away");
        pleague = infoObj.optString("league");
        pstarttime = infoObj.optString("time");
        pstatus = infoObj.optString("status");

        hometeam.setText(phometeam);
        awayteam.setText(pawayteam);
        homescore.setText(phomescore);
        awayscore.setText(pawayscore);
        league.setText(pleague);
        starttime.setText(pstarttime);
        status.setText(pstatus);

        }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_live_teams, container, false);
        setRetainInstance(true);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        hometeam = (TextView)view.findViewById(R.id.hometeam);
        awayteam = (TextView)view.findViewById(R.id.awayteam);
        homescore = (TextView)view.findViewById(R.id.homescore);
        awayscore= (TextView)view.findViewById(R.id.awayscore);
        league = (TextView)view.findViewById(R.id.league);
        starttime = (TextView)view.findViewById(R.id.starttime);
        status = (TextView)view.findViewById(R.id.status);

        //volley
        requestQueue =VolleySingleton.getInstance().getRequestQueue();

        LiveDetails ld = (LiveDetails) getActivity();
        api_id = ld.getApi_id();
        api_url = base_url+api_id;

        CacheRequest cacheRequest = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    progressBar.setVisibility(View.GONE);

                    try {
                            parseResult(jsonString);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                    //Toast.makeText(getActivity(), "No connection ", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getActivity(), "problem fetching data ", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Add the request to the RequestQueue.
        requestQueue.add(cacheRequest);
        return view;
    }


    //caching class
    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
